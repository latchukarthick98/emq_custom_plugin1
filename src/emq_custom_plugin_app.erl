%%%-------------------------------------------------------------------
%% @doc emq_custom_plugin public API
%% @end
%%%-------------------------------------------------------------------

-module(emq_custom_plugin_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
   {ok, Sup} = emq_custom_plugin_sup:start_link(),
   ok = emqttd_access_control:register_mod(auth, emq_auth_module, []),
   ok = emqttd_access_control:register_mod(acl, emq_acl_module, []),
   emq_custom_plugin:load(application:get_all_env()),
   {ok, Sup}.
%%--------------------------------------------------------------------
stop(_State) ->
    ok = emqttd_access_control:unregister_mod(auth, emq_auth_module),
    ok = emqttd_access_control:unregister_mod(acl, emq_acl_module),
    emq_custom_plugin:unload().

%%====================================================================
%% Internal functions
%%====================================================================
